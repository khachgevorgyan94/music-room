import React, {Component} from 'react';
import './style.scss'
import image1 from '../../assets/images/author-1.jpg'
import image2 from '../../assets/images/author-2.jpg'
import image3 from '../../assets/images/author-3.jpg'
import image4 from '../../assets/images/author-4.jpg'
import MusicBox from "./components/music-box";
import InformationBox from "./components/information-box";

class UsersList extends Component {
  // span   fixed  position for users  box active

  state = {
    selectedFilter: 1,
    isSelected: null,
    selectedBox: null,
    isOpen: false,
    offsetTop: {
      top: 0,
      count: 1
    }
  }

  data = [
    {
      id: 1,
      name: 'Test Testyan',
      photo: image1,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image1,
        image1,
        image1,
        image1,
        image1,
        image1,
      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 2,
      name: 'Test Testyan',
      photo: image2,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image2,
        image2,
        image2,
        image2,
        image2,
        image2,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image2,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 3,
      name: 'Test Testyan',
      photo: image3,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image3,
        image3,
        image3,
        image3,
        image3,
        image3,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image3,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 4,
      name: 'Test Testyan',
      photo: image4,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image4,
        image4,
        image4,
        image4,
        image4,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image4,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 5,
      name: 'Test Testyan',
      photo: image1,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image1,
        image1,
        image1,
        image1,
        image1,
        image1,
      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 6,
      name: 'Test Testyan',
      photo: image2,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image2,
        image2,
        image2,
        image2,
        image2,
        image2,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image2,
        }
      ]
    },
    {
      id: 7,
      name: 'Test Testyan',
      photo: image3,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image3,
        image3,
        image3,
        image3,
        image3,
        image3,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image3,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 8,
      name: 'Test Testyan',
      photo: image4,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image4,
        image4,
        image4,
        image4,
        image4,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image4,
        },
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 9,
      name: 'Test Testyan',
      photo: image1,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image1,
        image1,
        image1,
        image1,
        image1,
        image1,
      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image1,
        }
      ]
    },
    {
      id: 10,
      name: 'Test Testyan',
      photo: image2,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image2,
        image2,
        image2,
        image2,
        image2,
        image2,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image2,
        }
      ]
    },
    {
      id: 11,
      name: 'Test Testyan',
      photo: image3,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image3,
        image3,
        image3,
        image3,
        image3,
        image3,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image3,
        }
      ]
    },
    {
      id: 12,
      name: 'Test Testyan',
      photo: image4,
      position: 'Composer',
      dateOfBirth: '01/01/2021',
      instruments: [
        'test 1', 'test 2'
      ],
      photos: [
        image4,
        image4,
        image4,
        image4,
        image4,

      ],
      about:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam ' +
          'porro provident quibusdam tempora veniam veritatis. Adipisci asperiores dignissimos doloremque, ' +
          'dolorum est facilis iusto laboriosam neque qui suscipit.',
      projects: [
        {
          date: '01/01/2021',
          description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur earum fuga laboriosam',
          image: image4,
        }
      ]
    },
  ]

  componentDidMount() {

  }

  changeFilter = (selectedFilter) => {
    this.setState({selectedFilter})
  }

  selectBox = (item) => {
    if (item.id === this.state.isSelected) {
      this.setState({
        isOpen: false
      }, () => {
        setTimeout(() => {
          this.setState({
            isSelected: null,
            selectedBox: null,
          })
        }, 500)
      })
    } else {
      this.setState({
        selectedBox: item,
        isSelected: item.id,
        isOpen: true
      })
    }

  }
  getPositionUserBox = (position, index) => {
    let calcIndex = 0;
    this.data.forEach((item, itemIndex) => {
      if (index > 4 * itemIndex - 5 && (4 * itemIndex - 5) > 0) {
        calcIndex++
      }
    })
    window.scrollTo(0, 0)
    if (!this.state.isSelected) {
      let top = position.current.clientHeight
      position.current.style.transform = `translate(${-(position.current.offsetLeft - 25) + 'px'},${-(position.current.offsetTop - top / 10 + calcIndex * 120) + 'px'} )`;
      this.setState({
        offsetTop: {
          top: position.current.offsetTop - top / 10 + calcIndex * 120,
          count: calcIndex
        }
      })
      document.body.classList.add('G-hidden')
    } else {
      position.current.style.transform = `translate(${0 + 'px'},${0 + 'px'} )`
      document.body.classList.remove('G-hidden')

    }
  }

  render() {
    const {selectedFilter, selectBox} = this.state
    return (
        <div className='G-container'>
          <div className='P-users-block'>
            <div className='P-users-list-header G-flex G-align-center G-justify-between'>
              <div className='P-users-count'>
                <h3>{this.data.length} artist</h3>
                <span>in the room</span>
              </div>
              <div className='P-users-filter'>
                <ul className='G-flex G-align-center'>
                  <li onClick={() => this.changeFilter(1)}
                      className={selectedFilter === 1 ? 'P-active-filter' : ''}>Test
                  </li>
                  <li onClick={() => this.changeFilter(2)}
                      className={selectedFilter === 2 ? 'P-active-filter' : ''}>Test
                  </li>
                  <li onClick={() => this.changeFilter(3)}
                      className={selectedFilter === 3 ? 'P-active-filter' : ''}>Test
                  </li>
                  <li onClick={() => this.changeFilter(4)}
                      className={selectedFilter === 4 ? 'P-active-filter' : ''}>Test
                  </li>
                </ul>
              </div>
            </div>
            <div className='P-users-list G-flex G-flex-wrap'>
              {this.data.map((item, index) => {
                return <MusicBox
                    selectBox={this.selectBox}
                    selected={this.state.isSelected}
                    index={index}
                    positionBox={this.getPositionUserBox}
                    key={index}
                    item={item}/>
              })}
            </div>
          </div>
          <InformationBox isOpen={this.state.isOpen} offsetTop={this.state.offsetTop} data={this.state.selectedBox}/>
        </div>
    );
  }
}

export default UsersList;
