import React, {Component} from 'react';
import {createBrowserHistory} from 'history';
import {Switch, Route, Redirect, BrowserRouter} from 'react-router-dom';
import './assets/styles/index.scss';
import Home from "./modules/home";
import ROUTES from "./platform/routes";
import Header from "./components/header";
import Footer from "./components/footer";

//
class App extends Component {

  state = {};

  componentDidMount() {
    window.routerHistory = createBrowserHistory();
  }

  render() {
    return (
        <BrowserRouter>
          <Header/>
          <Switch>
            <Route path={ROUTES.HOME} component={Home} exact/>
            <Redirect to={ROUTES.HOME} exact={true}/>
          </Switch>
          <Footer/>
        </BrowserRouter>
    )
  }
}

export default App;
