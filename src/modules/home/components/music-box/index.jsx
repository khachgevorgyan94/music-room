import React, {Component} from 'react';
import './style.scss'

class MusicBox extends Component {

  itemBox = React.createRef()
  state = {
    isSelected: false,

    selectBoxId: null,
  }

  componentDidMount() {

  }

  // selectBox = (item) => {

  // }

  selectBox = () => {
    const {selectBox, positionBox, index} = this.props;
    positionBox(this.itemBox, index)
    selectBox(this.props.item)
  }

  render() {
    const {item, selected} = this.props
    return (
        <div ref={this.itemBox}
             className={`P-music-box-padding 
             ${selected && selected !== item.id ? 'P-animation-box' : ''}
             `}
             onClick={this.selectBox}>
          <div className='P-music-box'>
            <div className='P-image-overflow'>
              <div className='P-music-box-image G-image-cover' style={{backgroundImage: `url('${item.photo}')`}}/>

            </div>
            <div className='P-music-box-info'>
              <div className='P-music-box-info-main'>
                <h3>{item.name}</h3>
                <p>{item.position}</p>
              </div>
              <div className={`P-music-box-info-hidden ${selected ? 'P-shoe-info' : ''}`}>
                <h3>{item.dateOfBirth}</h3>
                <h2>Instruments</h2>
                <ul>
                  {item.instruments.map((item, index) => {
                    return <li key={index}>{item}</li>
                  })}
                </ul>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default MusicBox;
