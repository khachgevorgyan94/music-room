import React, {Component} from 'react';
import './style.scss'
import Slider from "react-slick";

class InformationBox extends Component {

  infoBox = React.createRef()

  componentDidUpdate() {
  }

  settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay:true
  };

  render() {
    const {data, isOpen} = this.props
    return (
        <div ref={this.infoBox} className={`P-user-information-box ${isOpen ? 'P-show-information' : ''}`}>
          <div className='P-slider-box'>
            <Slider {...this.settings}>
              {data && data.photos.map((item, index) => {
                return <div key={index} className='P-slider-images'>
                  <div className='P-slider-image G-image-cover' style={{backgroundImage: `url('${item}')`}}/>
                </div>
              })}
            </Slider>
          </div>
          {data &&
          <>
            <div className='P-user-information'>
              <h2>about {data.name}</h2>
              <p>{data.about}</p>
            </div>
            <div className='P-user-information'>
              <h2>projects with {data.name}</h2>
              <div className='P-users-projects G-flex G-flex-wrap'>
                {data.projects.map((item, index) => {
                  return <div key={index} className='P-user-projects-padding'>
                    <div className='P-projects-box G-image-cover' style={{backgroundImage:`url('${item.image}')`}}>
                      <span>{item.date}</span>
                      <p>{item.description}</p>
                    </div>
                  </div>
                })}
              </div>
            </div>
          </>
          }
        </div>
    );
  }
}

export default InformationBox;
